# Serv-status
Check if a list of hosts (http,https,ftp,...) is online or not and show it in a http server.
If some server goes offline send a telegram msg to you.

# Install

  ```sh
  npm install
  ```