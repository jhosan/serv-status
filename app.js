const isReachable = require('is-reachable')
const simplenodelogger = require("simple-node-logger")
var config = require('./config.js')
const express = require('express')
const exphbs = require('express-handlebars')

opts = {
	logFilePath:'status.log',
	timestampFormat:'YYYY-MM-DD HH:mm:ss'
},
log = simplenodelogger.createSimpleLogger( opts );
log.setLevel("error")


const port = 3008

const app = express()
app.engine('.hbs', exphbs({extname: '.hbs'}))
app.set('view engine', '.hbs')
app.set('views', __dirname)
app.use(express.static('static'))

const TeleBot = require('telebot')
const telegrambot = new TeleBot(config.telegram.token)



app.get('/', (req, res) => {
	res.render('status',  {'hosts':config.hosts});
})

function test(){
	log.debug('Making a test for every host....')
	config.hosts.forEach((value) =>{
		isReachable(value.host).then(reachable => {
			value.lastcheck = new Date().toLocaleString()
    		if(reachable){
				value.isonline = true;
				log.debug(`${value.name} is ONLINE!`)
			}
			else if(value.offlinei == 0 || !value.offlinei){
				value.isonline = false
				value.offlinei++
				log.error(`${value.name} is OFFLINE!`)
				telegrambot.sendMessage(config.telegram.chatid,`ATTENTION!!: ${value.name} is OFFLINE!!!`)
				if(value.offlinei > 5 ) { value.offlinei = 0 }
			}
		});
	})
}

test();
setInterval(test, 300000);

app.listen(port, () => log.debug(`Running!! --> Port: ${port}`))